﻿namespace Landmine
{
    using System;
    using Landmine.Domain.Models;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services.Interfaces;

    public class Application
    {
        private readonly IServiceQuery<NewBoardQuery, GameBoard> newBoardGenerator;
        private readonly IServiceQuery<ValidMoveQuery, bool> moveValidator;
        private readonly IServiceQuery<NewPositionQuery, Tuple<int, int>> newPositionQuery;
        private readonly IServiceQuery<MoveStateQuery, string> moveStateQuery;

        public Application(IServiceQuery<NewBoardQuery, GameBoard> newBoardGenerator,
        IServiceQuery<ValidMoveQuery, bool> moveValidator,
            IServiceQuery<NewPositionQuery, Tuple<int,int>> newPositionQuery,
            IServiceQuery<MoveStateQuery, string> moveStateQuery)
        {
            this.newBoardGenerator = newBoardGenerator;
            this.moveValidator = moveValidator;
            this.newPositionQuery = newPositionQuery;
            this.moveStateQuery = moveStateQuery;
        }

        public void Run()
        {
            Console.WriteLine("Please enter the maximum number of mines in the game (note that higher numbers mean it will take longer to generate the game board):");
            var maxMines = Console.ReadLine();

            //Get a board. You could later on make the dimension size configurable. We're assuming the user input is good here for the sake of brevity.
            var board = this.newBoardGenerator.Execute(new NewBoardQuery(8, int.Parse(maxMines)));

            Console.WriteLine("Generating board, please wait...");

            // Display the status as at the start of the game. hitMine will always be false at this point as we disallow 1,1 being a mine.
            Console.WriteLine(this.moveStateQuery.Execute(new MoveStateQuery(board, false)));

            // OK, so lets loop until mine hit count < 3 or current possition on the y axis is equal to the dimension size. Could also make this configurable later.
            while (board.MinesHitCount < 3 && board.CurrentPosition.Item2 < board.DimensionSize)
            {
                var hitMine = false;

                Console.WriteLine("Please enter move:");
                var move = Console.ReadLine();

                //We're assuming the input is good. Check if the move can be made.
                if (this.moveValidator.Execute(new ValidMoveQuery(move, board)))
                {
                    // Move is valid. Set current position to the new one and see if we hit a mine.
                    board.CurrentPosition = this.newPositionQuery.Execute(new NewPositionQuery(move, board.CurrentPosition));

                    // TODO this shouldn't be here - no testable logic should be in this class. Realised this whole block should probably be in a command as it is a state change op, but ran out of time.
                    if (board.MineSquares.Contains(board.CurrentPosition))
                    {
                        hitMine = true;
                        board.MinesHitCount += 1;
                    }
                }
                else
                {
                    Console.WriteLine("I'm sorry, but your move would be off the board. Please try again.");
                }

                Console.WriteLine(this.moveStateQuery.Execute(new MoveStateQuery(board, hitMine)));
            }

            Console.WriteLine("Please press any key to end the game.");
            Console.ReadKey();

        }
    }
}
