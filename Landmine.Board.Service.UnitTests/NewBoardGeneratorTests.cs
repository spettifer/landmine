﻿namespace Landmine.Board.Service.UnitTests
{
    using System;
    using System.Collections.Generic;
    using AutoFixture.Xunit2;
    using FluentAssertions;
    using Landmine.Domain.Interfaces;
    using Landmine.Domain.Models;
    using Moq;
    using Xunit;

    public class NewBoardGeneratorTests
    {
        [Theory]
        [InlineAutoData(8, 5)]
        public void GeneratorShouldCreateGameBoard(int dimension, int maxMines, Mock<IMineRandomiser> mineRandomiserMock)
        {
            // We'll have 5 mines.
            mineRandomiserMock.Setup(x => x.NumberOfLandmines(It.IsAny<int>(), It.IsAny<int>())).Returns(5);

            mineRandomiserMock.Setup(x => x.GenerateTupleItemValue(It.IsAny<int>()))
                .Returns(new Queue<int>(new[] {1, 2, 3, 4, 5, 6, 7, 8, 8, 7}).Dequeue);


            var generator = new NewBoardGenerator(mineRandomiserMock.Object);

            var actual = generator.Generate(dimension, maxMines);

            var expected = new GameBoard(dimension) {MineSquares = GetMockMinesList()};

            expected.Should().BeEquivalentTo(actual);
        }

        private List<Tuple<int, int>> GetMockMinesList()
        {
            return new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(1,2),
                new Tuple<int, int>(3,4),
                new Tuple<int, int>(5,6),
                new Tuple<int, int>(7,8),
                new Tuple<int, int>(8,7)
            };
        }
    }
}
