﻿namespace Landmine.Domain.Services.Interfaces
{
    using Landmine.Domain.Models;

    public interface INewBoardGenerator
    {
        GameBoard Generate(int dimensionSize, int maximumNumberOfMines);
    }
}
