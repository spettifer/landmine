﻿using System.Collections.Generic;

namespace Landmine.Domain.Models
{
    using System;

    public class GameBoard
    {
        public GameBoard(int dimensionSize)
        {
            this.DimensionSize = dimensionSize;
            this.CurrentPosition = new Tuple<int, int>(1, 1);
        }
        public int DimensionSize { get; }
        public List<Tuple<int,int>> MineSquares { get; set; }
        public Tuple<int, int> CurrentPosition { get; set; }
        public int MinesHitCount { get; set; }
    }
}
