﻿using System.Reflection;
using Autofac;
using Landmine.Domain;
using Landmine.Domain.Services.Interfaces;

namespace Landmine
{
    class Program
    {
        private static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(Landmine.Domain.AssemblyHook))).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(AssemblyHook)))
                .AsClosedTypesOf(typeof(IServiceQuery<,>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(Landmine.Board.Service.AssemblyHook))).AsImplementedInterfaces().InstancePerLifetimeScope();
            return builder.Build();
        }

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        static void Main()
        {
            // Get an instance of the application to run so we can use DI to do our heavy lifting.
            CompositionRoot().Resolve<Application>().Run();
        }
    }
}
