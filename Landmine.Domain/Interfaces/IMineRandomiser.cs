﻿namespace Landmine.Domain.Interfaces
{
    public interface IMineRandomiser
    {
        int NumberOfLandmines(int boardDimensionSize, int maxNumberOfMines);

        int GenerateTupleItemValue(int boardDimensionSize);
    }
}
