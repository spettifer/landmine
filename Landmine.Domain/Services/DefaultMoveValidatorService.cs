﻿namespace Landmine.Domain.Services
{
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services.Interfaces;

    public class DefaultMoveValidatorService : IServiceQuery<ValidMoveQuery, bool>
    {
        public bool Execute(ValidMoveQuery query)
        {
            var currentX = query.Board.CurrentPosition.Item1;
            var currentY = query.Board.CurrentPosition.Item2;

            switch (query.Move.ToUpper())
            {
                case "U":
                    return currentY + 1 <= query.Board.DimensionSize;
                case "D":
                    return currentY - 1 > 0;
                case "L":
                    return currentX - 1 > 0;
                case "R":
                    return currentX + 1 <= query.Board.DimensionSize;
            }

            return false;
        }
    }
}
