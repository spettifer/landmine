﻿namespace Landmine.Domain.Services
{
    using System;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services.Interfaces;

    public class DefaultNewPositionService : IServiceQuery<NewPositionQuery, Tuple<int,int>>
    {
        public Tuple<int, int> Execute(NewPositionQuery query)
        {
            Tuple<int,int> newPosisiton = null;

            switch (query.Move.ToUpper())
            {
                case "U":
                    newPosisiton = new Tuple<int, int>(query.CurrentPosition.Item1, query.CurrentPosition.Item2 + 1);
                    break;
                case "D":
                    newPosisiton = new Tuple<int, int>(query.CurrentPosition.Item1, query.CurrentPosition.Item2 - 1);
                    break;
                case "L":
                    newPosisiton = new Tuple<int, int>(query.CurrentPosition.Item1 - 1, query.CurrentPosition.Item2);
                    break;
                case "R":
                    newPosisiton = new Tuple<int, int>(query.CurrentPosition.Item1 + 1, query.CurrentPosition.Item2);
                    break;
            }

            return newPosisiton;
        }
    }
}
