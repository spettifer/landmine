﻿namespace Landmine.Domain.UnitTests
{
    using FluentAssertions;
    using Landmine.Domain.Shared;
    using Xunit;

    public class MineRandomiserTests
    {
        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(10)]
        [InlineData(70)]
        public void NumberOfMinesShouldNotExceedSpecifiedMaximum(int maxMines)
        {
            var randomiser = new MineRandomiser();

            // For completeness you should also test for dimension size too, but time is ticking.
            var actual = randomiser.NumberOfLandmines(8, maxMines);

            actual.Should().BeLessOrEqualTo(maxMines);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(10)]
        public void NumberOfMinesShouldNotExceedDimensionSquaredMinusOne(int dimensionSize)
        {
            var randomiser = new MineRandomiser();

            // Make the max number exceed dimension size squared so that it won't influence the outcome.
            var actual = randomiser.NumberOfLandmines(dimensionSize, dimensionSize*dimensionSize + 1);

            actual.Should().BeLessOrEqualTo(dimensionSize * dimensionSize + 1);
        }
    }
}
