﻿namespace Landmine.Domain.UnitTests
{
    using System;
    using AutoFixture.Xunit2;
    using FluentAssertions;
    using Landmine.Domain.Models;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services;
    using Xunit;

    public class DefaultMoveValidatorServiceTests
    {
        [Theory]
        [InlineAutoData("X")]
        public void UnknownMoveShouldReturnFalse(string move, GameBoard board)
        {
            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeFalse("We should not process an unknown command.");
        }

        [Theory]
        [InlineAutoData("R")]
        public void MoveExceedingXAxisUpperLimitShouldReturnFalse(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(board.DimensionSize, 1);

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeFalse("We cannot move outside the bounds of the game board.");
        }

        [Theory]
        [InlineAutoData("U")]
        public void MoveExceedingYAxisUpperLimitShouldReturnFalse(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, board.DimensionSize);

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeFalse("We cannot move outside the bounds of the game board.");
        }

        [Theory]
        [InlineAutoData("L")]
        public void MoveExceedingXAxisLowerLimitShouldReturnFalse(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, 1);

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeFalse("We cannot move outside the bounds of the game board.");
        }

        [Theory]
        [InlineAutoData("D")]
        public void MoveExceedingYAxisLowerLimitShouldReturnFalse(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, 1);

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeFalse("We cannot move outside the bounds of the game board.");
        }

        [Theory]
        [InlineAutoData("L")]
        [InlineAutoData("R")]
        public void MoveWhichResultsInValidPositionOnXAxisShouldReturnTrue(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>((int)Math.Round((decimal)board.DimensionSize/2), 1);

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeTrue();
        }

        [Theory]
        [InlineAutoData("U")]
        [InlineAutoData("D")]
        public void MoveResultsInValidPositionOnYAxisShouldReturnTrue(string move, GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, (int)Math.Round((decimal)board.DimensionSize / 2));

            var validator = new DefaultMoveValidatorService();

            var actual = validator.Execute(new ValidMoveQuery(move, board));

            actual.Should().BeTrue();
        }
    }
}
