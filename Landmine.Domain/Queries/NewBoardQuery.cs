﻿namespace Landmine.Domain.Queries
{
    public class NewBoardQuery
    {
        public NewBoardQuery(int dimensionSize, int maximumNumberOfMines)
        {
            // Should validate here but assuming happy path
            this.DimensionSize = dimensionSize;
            this.MaximumNumberOfMines = maximumNumberOfMines;
        }

        public int DimensionSize { get; }

        public int MaximumNumberOfMines { get; }
    }
}
