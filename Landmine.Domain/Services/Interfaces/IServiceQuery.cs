﻿namespace Landmine.Domain.Services.Interfaces
{
    /// <summary>
    /// Service query
    /// </summary>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IServiceQuery<in TQuery, out TResult>
    {
        /// <summary>
        /// Executes the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The query result</returns>
        TResult Execute(TQuery query);
    }
}
