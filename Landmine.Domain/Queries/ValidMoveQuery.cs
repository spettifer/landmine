﻿using Landmine.Domain.Models;

namespace Landmine.Domain.Queries
{
    public class ValidMoveQuery
    {
        public ValidMoveQuery(string move, GameBoard board)
        {
            this.Move = move;
            this.Board = board;
        }
        public string Move { get; }

        public GameBoard Board { get; }
    }
}
