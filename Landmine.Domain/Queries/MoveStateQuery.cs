﻿namespace Landmine.Domain.Queries
{
    using Landmine.Domain.Models;

    public class MoveStateQuery
    {
        public MoveStateQuery(GameBoard board, bool hitMine)
        {
            this.Board = board;
            this.HitMine = hitMine;
        }

        public GameBoard Board { get; }

        public bool HitMine { get; }
    }
}
