﻿namespace Landmine.Domain.Shared
{
    using System;
    using Landmine.Domain.Interfaces;

    /// <summary>
    /// This class alows us to mock the random number generator for testing.
    /// </summary>
    /// <seealso cref="IMineRandomiser" />
    public class MineRandomiser : IMineRandomiser
    {
        public int NumberOfLandmines(int boardDimensionSize, int maxNumberOfMines)
        {
            var rnd = new Random();
            return rnd.Next(1, Math.Min(boardDimensionSize * boardDimensionSize, maxNumberOfMines));   // We'll take the smaller of max board size or the specified maximum number of mines as our upper limit
        }

        public int GenerateTupleItemValue(int boardDimensionSize)
        {
            var rnd = new Random();
            return rnd.Next(1, boardDimensionSize + 1);
        }
    }
}
