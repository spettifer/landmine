﻿namespace Landmine.Domain.Services
{
    using System.Text;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services.Interfaces;

    public class DefaultMoveStateService : IServiceQuery<MoveStateQuery, string>
    {
        public string Execute(MoveStateQuery query)
        {
            var builder = new StringBuilder();

            builder.Append($"Current position: {query.Board.CurrentPosition.Item1}, {query.Board.CurrentPosition.Item2} | ");
            builder.Append($"You {(query.HitMine ? string.Empty : "didn't ")}hit a mine | ");
            builder.Append($"Total mines hit: {query.Board.MinesHitCount}");
            builder.Append($" {(query.Board.CurrentPosition.Item2 == query.Board.DimensionSize ? " | Congratulations! You won!" : string.Empty)}");
            builder.Append($"{(query.Board.MinesHitCount > 2 ? " | Sorry, you hit too many mines this time." : string.Empty)}");

            return builder.ToString();
        }
    }
}
