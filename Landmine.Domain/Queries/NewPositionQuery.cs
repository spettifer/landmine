﻿using System;
using System.Runtime.InteropServices;

namespace Landmine.Domain.Queries
{
    public class NewPositionQuery
    {
        public NewPositionQuery(string move, Tuple<int,int> currentPosition)
        {
            this.Move = move;
            this.CurrentPosition = currentPosition;
        }

        public Tuple<int,int> CurrentPosition { get; }

        public string Move { get; }
    }
}
