﻿namespace Landmine.Domain.Services
{
    using Landmine.Domain.Models;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services.Interfaces;

    public class DefaultNewBoardService : IServiceQuery<NewBoardQuery, GameBoard>
    {
        private readonly INewBoardGenerator newBoardGenerator;

        public DefaultNewBoardService(INewBoardGenerator newBoardGenerator)
        {
            this.newBoardGenerator = newBoardGenerator;
        }

        public GameBoard Execute(NewBoardQuery query)
        {
            return newBoardGenerator.Generate(query.DimensionSize, query.MaximumNumberOfMines);
        }
    }
}
