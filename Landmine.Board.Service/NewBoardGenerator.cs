﻿namespace Landmine.Board.Service
{
    using System;
    using System.Collections.Generic;
    using Landmine.Domain.Interfaces;
    using Landmine.Domain.Models;
    using Landmine.Domain.Services.Interfaces;

    public class NewBoardGenerator : INewBoardGenerator
    {
        private readonly IMineRandomiser mineRandomiser;

        public NewBoardGenerator(IMineRandomiser mineRandomiser)
        {
            this.mineRandomiser = mineRandomiser;
        }

        public GameBoard Generate(int dimensionSize, int maximumNumberOfMines)
        {
            var numberOfLandmines = this.mineRandomiser.NumberOfLandmines(dimensionSize, maximumNumberOfMines);

            var mineSquares = new List<Tuple<int, int>>();

            // This is quite rudimentary and not very efficient, but it demonstrates the point.
            for (var i = 1; i <= numberOfLandmines; i++)
            {
                var square = new Tuple<int, int>(this.mineRandomiser.GenerateTupleItemValue(dimensionSize), this.mineRandomiser.GenerateTupleItemValue(dimensionSize));

                // Prevent the starting square from being a mine. Bit unfair otherwise!
                while (mineSquares.Contains(square) || square.Item1 == 1 && square.Item2 == 1)
                {
                    square = new Tuple<int, int>(this.mineRandomiser.GenerateTupleItemValue(dimensionSize), this.mineRandomiser.GenerateTupleItemValue(dimensionSize));
                }

                mineSquares.Add(square);
            }

            return new GameBoard(dimensionSize) { MineSquares = mineSquares};
        }
    }
}
