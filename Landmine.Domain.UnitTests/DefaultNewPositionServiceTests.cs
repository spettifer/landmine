﻿namespace Landmine.Domain.UnitTests
{
    using System;
    using AutoFixture.Xunit2;
    using FluentAssertions;
    using Landmine.Domain.Models;
    using Landmine.Domain.Queries;
    using Landmine.Domain.Services;
    using Xunit;

    public class DefaultNewPositionServiceTests
    {
        [Theory]
        [AutoData]
        public void MoveLeftShouldDecrementCurrentPositionTupleItem1ByOne(GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(board.DimensionSize, 1);

            var validator = new DefaultNewPositionService();

            var actual = validator.Execute(new NewPositionQuery("L", board.CurrentPosition));

            actual.Should().BeEquivalentTo(new Tuple<int,int>(board.DimensionSize - 1, 1));
        }

        [Theory]
        [AutoData]
        public void MoveRightShouldIncrementCurrentPositionTupleItem1ByOne(GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, 1);

            var validator = new DefaultNewPositionService();

            var actual = validator.Execute(new NewPositionQuery("R", board.CurrentPosition));

            actual.Should().BeEquivalentTo(new Tuple<int, int>(2, 1));
        }

        [Theory]
        [AutoData]
        public void MoveUpShouldIncrementCurrentPositionTupleItem2ByOne(GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, 1);

            var validator = new DefaultNewPositionService();

            var actual = validator.Execute(new NewPositionQuery("U", board.CurrentPosition));

            actual.Should().BeEquivalentTo(new Tuple<int, int>(1, 2));
        }

        [Theory]
        [AutoData]
        public void MoveDownShouldDecrementCurrentPositionTupleItem2ByOne(GameBoard board)
        {
            board.CurrentPosition = new Tuple<int, int>(1, board.DimensionSize);

            var validator = new DefaultNewPositionService();

            var actual = validator.Execute(new NewPositionQuery("D", board.CurrentPosition));

            actual.Should().BeEquivalentTo(new Tuple<int, int>(1, board.DimensionSize - 1));
        }
    }
}
